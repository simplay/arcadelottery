import os
import random
import pygame
import sys
from pygame.locals import *

from pathlib import Path

from arcadelottery.buttons import ButtonListener, ButtonState

SECOND_PRICE = "1"

# Constants for GUI
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 450
SCREEN_TITLE = "ARCADE"

ROOT_PATH = Path(__file__).parent.parent

# Constants for GUI
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 450

TREASURE_1 = pygame.image.load("images/treasure1.png")
TREASURE_2 = pygame.image.load("images/treasure1.png")
TREASURE_3 = pygame.image.load("images/treasure1.png")

STARTING_RECT_TREASURE_1 = TREASURE_1.get_rect()
STARTING_RECT_TREASURE_1.x = 145
STARTING_RECT_TREASURE_1.y = 290

STARTING_RECT_TREASURE_2 = TREASURE_2.get_rect()
STARTING_RECT_TREASURE_2.x = 335
STARTING_RECT_TREASURE_2.y = 290

STARTING_RECT_TREASURE_3 = TREASURE_3.get_rect()
STARTING_RECT_TREASURE_3.x = 520
STARTING_RECT_TREASURE_3.y = 290

BOMB_IMAGE = pygame.image.load("images/bomb.png")
BOMB = BOMB_IMAGE.get_rect()
BOMB.x = 335 + 40
BOMB.y = 130

PRICE_ASSETS = {}
for idx in range(20):
    price_image = pygame.image.load(f"images/numbers/{idx + 1}.png")
    price_asset = price_image.get_rect()
    price_asset.x = 335 + 30
    price_asset.y = 95

    PRICE_ASSETS[str(idx + 1)] = {
        "img": price_image,
        "bounding_box": price_asset
    }

BACKGROUND = pygame.image.load("images/background.png")
BLACK = 0, 0, 0


def go_crazy(screen):
    start_ticks = pygame.time.get_ticks()
    base_speed = 32
    speed1 = [-base_speed, 0]
    speed2 = [base_speed, 0]
    speed3 = [-base_speed, 0]
    seconds = 0

    treasure_rect = STARTING_RECT_TREASURE_1
    treasure_rect2 = STARTING_RECT_TREASURE_2
    treasure_rect3 = STARTING_RECT_TREASURE_3

    play_shuffle_sound()
    while seconds < 3:
        seconds = (pygame.time.get_ticks() - start_ticks) / 1000

        treasure_rect = treasure_rect.move(speed1)
        if treasure_rect.left < 50 or treasure_rect.right > SCREEN_WIDTH - 50:
            speed1[0] = -speed1[0]

        treasure_rect2 = treasure_rect2.move(speed2)
        if treasure_rect2.left < 50 or treasure_rect2.right > SCREEN_WIDTH - 50:
            speed2[0] = -speed2[0]

        treasure_rect3 = treasure_rect3.move(speed3)
        if treasure_rect3.left < 50 or treasure_rect3.right > SCREEN_WIDTH - 50:
            speed3[0] = -speed3[0]

        screen.fill(BLACK)
        screen.blit(BACKGROUND, BACKGROUND.get_rect())
        screen.blit(TREASURE_1, treasure_rect)
        screen.blit(TREASURE_2, treasure_rect2)
        screen.blit(TREASURE_3, treasure_rect3)
        pygame.display.flip()

    screen.fill(BLACK)
    screen.blit(BACKGROUND, BACKGROUND.get_rect())
    screen.blit(TREASURE_1, STARTING_RECT_TREASURE_1)
    screen.blit(TREASURE_2, STARTING_RECT_TREASURE_2)
    screen.blit(TREASURE_3, STARTING_RECT_TREASURE_3)
    pygame.display.flip()
    pygame.time.wait(200)


def animate_treasure(screen, button_name: str):
    if button_name == "green":
        treasure1 = pygame.image.load("images/treasure2.png")
        screen.blit(treasure1, STARTING_RECT_TREASURE_1)
        pygame.display.flip()
        pygame.time.wait(200)

        treasure1 = pygame.image.load("images/treasure3.png")
        screen.blit(treasure1, STARTING_RECT_TREASURE_1)
        pygame.display.flip()
        pygame.time.wait(300)

        screen.blit(TREASURE_1, STARTING_RECT_TREASURE_1)
        pygame.display.flip()
        pygame.time.wait(100)
    elif button_name == "blue":
        treasure2 = pygame.image.load("images/treasure2.png")
        screen.blit(treasure2, STARTING_RECT_TREASURE_2)
        pygame.display.flip()
        pygame.time.wait(200)

        treasure2 = pygame.image.load("images/treasure3.png")
        screen.blit(treasure2, STARTING_RECT_TREASURE_2)
        pygame.display.flip()
        pygame.time.wait(300)

        screen.blit(TREASURE_2, STARTING_RECT_TREASURE_2)
        pygame.display.flip()
        pygame.time.wait(100)
    elif button_name == "red":
        treasure3 = pygame.image.load("images/treasure2.png")
        screen.blit(treasure3, STARTING_RECT_TREASURE_3)
        pygame.display.flip()
        pygame.time.wait(200)

        treasure3 = pygame.image.load("images/treasure3.png")
        screen.blit(treasure3, STARTING_RECT_TREASURE_3)
        pygame.display.flip()
        pygame.time.wait(300)

        screen.blit(TREASURE_3, STARTING_RECT_TREASURE_3)
        pygame.display.flip()
        pygame.time.wait(100)


def play_shuffle_sound():
    audio_filepath = os.path.join(ROOT_PATH, "sounds", "shuffle.wav")
    track = pygame.mixer.Sound(audio_filepath)
    pygame.mixer.Sound.play(track)


def play_win_sound():
    audio_filepath = os.path.join(ROOT_PATH, "sounds", "user_win.wav")
    track = pygame.mixer.Sound(audio_filepath)
    pygame.mixer.Sound.play(track)


def play_error_sound():
    audio_filepath = os.path.join(ROOT_PATH, "sounds", "error.wav")
    track = pygame.mixer.Sound(audio_filepath)
    pygame.mixer.Sound.play(track)


def play_gameover_theme():
    audio_filepath = os.path.join(ROOT_PATH, "sounds", "lost.mp3")
    pygame.mixer.music.load(audio_filepath)
    pygame.mixer.music.play()
    pygame.event.wait()


def render_start(screen):
    screen.fill(BLACK)
    screen.blit(BACKGROUND, BACKGROUND.get_rect())
    screen.blit(TREASURE_1, STARTING_RECT_TREASURE_1)
    screen.blit(TREASURE_2, STARTING_RECT_TREASURE_2)
    screen.blit(TREASURE_3, STARTING_RECT_TREASURE_3)
    pygame.display.flip()
    pygame.time.wait(200)


def render_bomb(screen):
    screen.blit(BOMB_IMAGE, BOMB)
    pygame.display.flip()
    pygame.time.wait(200)


def write_current_prices_file(remaining_prices: list):
    remaining_prices_filepath = os.path.join(ROOT_PATH, "data", "remaining_prices.txt")

    sorted_remaining_prices = sorted(remaining_prices, key=lambda x: int(x), reverse=False)
    with open(remaining_prices_filepath, "w") as file:
        lines = [f"{remaining_price}\n" for remaining_price in sorted_remaining_prices]
        file.writelines(lines)

    print("Remaining prices file was written.")


def main():
    button_state = ButtonState()
    ButtonListener(button_state).initialize()
    is_idle = True

    print("Starting Game")

    button_values = {
        "green": 0,
        "blue": 1,
        "red": 2
    }

    rects = {
        "green": STARTING_RECT_TREASURE_1,
        "blue": STARTING_RECT_TREASURE_2,
        "red": STARTING_RECT_TREASURE_3
    }

    pygame.init()

    audio_theme_filepath = os.path.join(ROOT_PATH, "sounds", "theme.mp3")

    # initialize frequency, size, channels, buffersize
    pygame.mixer.pre_init(44100, 16, 2, 4096)
    pygame.mixer.music.load(audio_theme_filepath)
    pygame.mixer.music.play(-1, 0.0)
    pygame.mixer.music.set_volume(0.35)

    screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])

    render_start(screen)

    available_prices = load_prices()
    first_time = True
    game_over = False

    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

        if len(available_prices) == 0:
            if not game_over:
                print("No prices left - please restart the game")
                pygame.mixer.music.pause()
                play_gameover_theme()
                game_over = True

                render_bomb(screen)

            continue

        if first_time:
            first_time = False
            # is_idle = False
            # go_crazy(screen)

        elif button_state.value:
            current_button_name = button_state.pretty_name()
            if current_button_name == "white":
                is_idle = False
                go_crazy(screen)
                button_state.reset()
                print("Game restarted... Please press either the red-, blue or green button.")

            elif not is_idle and current_button_name is not "none":
                print("Pressed button: ", current_button_name)

                button_pressed_value = button_values[current_button_name]
                treasure_rect = rects[current_button_name]

                prices = assemble_three_prices(available_prices)
                price = prices[button_pressed_value]

                play_win_sound()
                animate_treasure(screen, current_button_name)

                has_selected_purple_star = render_price_animation(price, screen, treasure_rect)
                if has_selected_purple_star:
                    available_prices.remove(price)

                print("price: ", price, " from available: ", available_prices, " prices: ", prices)
                write_current_prices_file(available_prices)

                is_idle = True
                button_state.reset()

                print("Round is over. Please press the white button to restart the game")

            elif is_idle and current_button_name is not "none":
                play_error_sound()


def assemble_three_prices(prices) -> list:
    random.shuffle(prices)

    # get last element of shuffled available prices and compile list of 3 prices used for that mini game.
    random_price = prices[-1]
    three_prices = [SECOND_PRICE, SECOND_PRICE, random_price]
    random.shuffle(three_prices)

    return three_prices


def load_prices() -> list:
    filepath = os.path.join(ROOT_PATH, "data", "prices.txt")
    with open(filepath, "r") as file:
        all_prices = [line.strip() for line in file.readlines()]

    return all_prices


def print_star(screen, treasure_rect, starcolor: str, price_name: str):
    for i in range(3):
        counter = 1
        for x in range(6):
            star_png_filepath = os.path.join(ROOT_PATH, "images", f"staranimation{counter}{starcolor}.png")
            purple_star = pygame.image.load(star_png_filepath)
            screen.fill(BLACK)
            screen.blit(BACKGROUND, BACKGROUND.get_rect())
            screen.blit(TREASURE_1, STARTING_RECT_TREASURE_1)
            screen.blit(TREASURE_2, STARTING_RECT_TREASURE_2)
            screen.blit(TREASURE_3, STARTING_RECT_TREASURE_3)

            asset = PRICE_ASSETS[price_name]
            screen.blit(asset["img"], asset["bounding_box"])

            purple_star_rect = purple_star.get_rect()
            purple_star_rect.x = treasure_rect.x + 25
            purple_star_rect.y = treasure_rect.y - 100

            screen.blit(purple_star, purple_star_rect)
            pygame.display.flip()
            pygame.time.wait(100)
            counter = counter + 1

    # run idle action for 6 seconds
    start_ticks = pygame.time.get_ticks()
    seconds = 0
    while seconds < 6:
        seconds = (pygame.time.get_ticks() - start_ticks) / 1000

    render_start(screen)


def render_price_animation(price: str, screen, treasure_rect) -> bool:
    """
    :param price:
    :param screen:
    :param treasure_rect:
    :return: return true if we got the purple star
    """

    # Here, you can adjust the probability for the 2nd price
    dice_roll_value = random.randint(0, 99)
    if price == SECOND_PRICE:
        print("You have won a regular price (direct): ", SECOND_PRICE)
        print_star(screen, treasure_rect, starcolor="", price_name=SECOND_PRICE)
        return False
    elif dice_roll_value < 25:
        print("You have won a regular price (dice): ", SECOND_PRICE)
        print_star(screen, treasure_rect, starcolor="", price_name=SECOND_PRICE)
        return False
    else:
        print("You have won a special price: ", price)
        print_star(screen, treasure_rect, starcolor="purple", price_name=price)
        return True


if __name__ == '__main__':
    main()
