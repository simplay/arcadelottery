# TODO: make this an object which is run within a thread.
#  The thread has a list of subscribers which can be notified in case certain events occured.

import os
import time
import RPi.GPIO as gpio
from evdev import uinput, UInput, ecodes as e
from smbus import SMBus


class ButtonState:
    states = {
        45055: "none",
        45051: "blue",
        45039: "white",
        45047: "green",
        45053: "red"
    }

    def __init__(self):
        self.value = None

    def update(self, value):
        self.value = value

    def reset(self):
        self.value = None

    def pretty_name(self):
        try:
            return self.states[self.value]
        except KeyError:
            return f"{self.value} (unknown)"


class ButtonListener:
    i2c_address = 0x26  # I2C Address of MCP23017
    irq_pin = 17  # IRQ pin for MCP23017
    IODIRA = 0x00
    IOCONA = 0x0A
    INTCAPA = 0x10

    def __init__(self, button_state: ButtonState):
        self.bus = None
        self.old_state = None
        self.button_state = button_state

    def initialize(self):
        os.system("sudo modprobe uinput")
        self.bus = SMBus(1)

        # Initial MCP23017 config:
        self.bus.write_byte_data(self.i2c_address, 0x05, 0x00)  # If bank 1, switch to 0
        self.bus.write_byte_data(self.i2c_address, self.IOCONA, 0x44)  # Bank 0, INTB=A, seq, OD IRQ

        # Read/modify/write remaining MCP23017 config:
        cfg = self.bus.read_i2c_block_data(self.i2c_address, self.IODIRA, 14)
        cfg[0] = 0xFF  # Input bits
        cfg[1] = 0xFF
        cfg[2] = 0x00  # Polarity
        cfg[3] = 0x00
        cfg[4] = 0xFF  # Interrupt pins
        cfg[5] = 0xFF
        cfg[12] = 0xFF  # Pull-ups
        cfg[13] = 0xFF
        self.bus.write_i2c_block_data(self.i2c_address, self.IODIRA, cfg)

        # Clear interrupt by reading INTCAP and GPIO registers
        x = self.bus.read_i2c_block_data(self.i2c_address, self.INTCAPA, 4)
        self.old_state = x[2] | (x[3] << 8)

        # GPIO init
        gpio.setwarnings(False)
        gpio.setmode(gpio.BCM)

        # Enable pullup and callback on MCP23017 IRQ pin
        gpio.setup(self.irq_pin, gpio.IN, pull_up_down=gpio.PUD_UP)
        gpio.add_event_detect(self.irq_pin, gpio.FALLING, callback=self.mcp_irq)

    # Callback for MCP23017 interrupt request
    def mcp_irq(self, pin):
        x = self.bus.read_i2c_block_data(self.i2c_address, self.INTCAPA, 4)
        new_state = x[2] | (x[3] << 8)
        for i in range(16):
            bit = 1 << i
            lvl = new_state & bit
            if lvl != (self.old_state & bit):
                self.button_state.update(new_state)

        self.old_state = new_state
