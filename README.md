# Arcade Lottery

## Setup

Be sure to enable I2C via `raspi-config`.

Next, install the following dependencies:

```
sudo apt-get install -y python-pip python-smbus python-dev libgeos++ gstreamer-1.0 python3-evdev
pip3 install -r requirements.txt
```

## Usage

`python3 run.py`